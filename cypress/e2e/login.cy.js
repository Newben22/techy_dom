import {login} from "../support/commands.js";

Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})
describe("Given I am want to login to my account", function () {
    before(function () {
        cy.visit('/')

    });
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    let number = parseInt(Math.random()*10000000, 10);
    let phone_number = '0804' + number;
    let email = phone_number + '@yopmail.com';
    let firstName_txt = 'Opeyemi' + number
    let lastName_txt = 'Techy' + number

    it("LOGIN - I Should be able to test this account", function () {
      cy.get(login.emailField).type(login.emailTxt)
      cy.get(login.passwordField).type(login.passwordTxt)
      cy.get(login.loginBtn).click()
      
    })
})