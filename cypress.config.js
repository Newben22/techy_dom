const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
 "baseUrl": "https://webmail.migadu.com/",
 "defaultCommandTimeout": 8000,
 "pageLoadTimeout": 100000,
 "viewportHeight": 800,
 "viewportWidth": 1500
}
});